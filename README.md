# BBEN

The Business Building Empowerment Network (06/2017)

# Description

International Business Network where SME businesses can access solutions for more growth development expansion effective operations and higher level business success.

# Goals

BBEN International Business support and serve to build number of businesses higher level success so they can serve even more better more pleasant life style for society.

# Services Required

Marketing solutions, cashflow solutions, busi- ness advice solutions (online/offline), network building solutions, leadership opportunities, charity.

# Website purpose

For personal use – support business grow, develop- ment, expansion, progress, higher level success for societies more pleasant benefits.
Advertise a service or product to members of the business customers, also, could be other busi- ness leaders.
To encourage potential customers to contact you by phone, email or post – In the first stage it would be only emails and face to face meetings. Call centre may be established in the future.
To make available product information and price lists – basic product information could be published, also, guides, videos, ebooks, journals etc. All the payment process could be implemented using well known payment systems. Commission share would be implemented. For example, Amazon ebook idea could be used.
Establish an online presence – Page ranking, search engine optimisation, filtering information in differ- ent libraries, directory system. Help to establish trust and branding between clients, another business leaders.
Provide information, for example, they could access articles written by different leaders, also, they could access the tips on how built a product successfully.
Provide a service, such as, marketing, funding, network building, advice from key experts website, sales company, marketing company, accountancy etc.

# Search engine keywords

Business success, business development, business grow, leadership, uk economy growth, uk employment growth, business advice, mentorship, marketing, sales, commercial legal, website solutions, accountancy firm, legal solutions.

# Development lifecycle

Layout 1 - The main idea is represented in the following link (https://www.bbenbusinessbuilding.co.uk).
Last platform change: Sunday, January 14, 2018 20:41

Layout 2 - The marketing page is represent in the following link (https://www.marketing.bbenbusinessbuilding.co.uk) 
Last platform change: Tuesday, April 10, 2018 23:59

As Wordpress platform was created originaly for BLOGS, CodeIgniter framework was adviced for commercial purpose (http://www.code.bbenbusinessbuilding.co.uk)
Last platform change: Saturday, October 20, 2018 13:12

# Plan

1. To learn how to use this framework for CodeIgniter platform.
2. Find out about hosting, domain etc.
3. Write notes as we go so others are aware how to use this and other platforms.

# Communications

Owner - Dwight Harrison Entrepreneur (videoproducedmarketing@gmail.com)

Developers - Daniel Andrukonis (mr.daniel.andrukonis@gmail.com), Prity Agnihotri (pritygirlz.kumari9@gmail.com), Ismaeel Shuaib (ismaeel93.is@gmail.com), Uday Sanisetty (udaysanisetty@gmail.com), Ezekiel Azi Shapiro Arin (ezekielarin@gmail.com)

    


